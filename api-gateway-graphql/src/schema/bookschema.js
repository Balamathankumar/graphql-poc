const { gql } = require('apollo-server-express');

const BooksSearchTypeDefs = gql`
    
    type Author {
       name: String!
    }

    type Category {
       name: String!
    }

    interface BookFields {
       
       title: String!
       authors: [Author]!
       isbn: String
       shortDescription: String
       longDescription: String
       status: String
       thumbnailUrl: String
       categories: [Category]
    }

    type Book implements BookFields { 
        title: String!
        authors: [Author]!
        isbn: String
        shortDescription: String
        longDescription: String
        status: String
        thumbnailUrl: String
        categories: [Category]
    }

    type RecommendBookByAuthor implements BookFields {
        title: String!
        authors: [Author]!
        isbn: String
        shortDescription: String
        longDescription: String
        status: String
        thumbnailUrl: String
        categories: [Category]
    }

    type RecommendBookByCategory implements BookFields {
        title: String!
        authors: [Author]!
        isbn: String
        shortDescription: String
        longDescription: String
        status: String
        thumbnailUrl: String
        categories: [Category]
    }
    
    `;

module.exports = {
    BooksSearchTypeDefs
};