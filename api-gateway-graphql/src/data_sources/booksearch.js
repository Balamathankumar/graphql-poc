const { RESTDataSource } = require('apollo-datasource-rest');
const { Client } = require('@elastic/elasticsearch');

class BooksSearchApi extends RESTDataSource {
  constructor() {
    super();
    this.client = new Client({ node: process.env.ELASTIC_URL })
  }
  
  async getBooks(filter, params, limitRows = { offset: 1, limit: 10}, sortRows = {sort: "title", sortBy: "ASC"}) {
    
    let query = {
      "query":{
        "match_all": { 
        }
      }
    }
    
    if (typeof filter != undefined) {
      query = {
        "query":{
          "multi_match": {
            "query":    filter.query,
            "fields": [ "title", "shortDescription", "longDescription" ] 
          }
        }
      }
    }

    if (typeof params != undefined && params.title != "") {
      query = {
        "query": {
            "term": {
                "title.keyword": {
                    "value": params.title,
                    "boost": 1.0
                }
            }
        }
    }
    }
  
    const { body } = await this.client.search({
      index: 'book',
      type: 'books',
      from: limitRows.offset,
      size: limitRows.limit,
      body: query
    })


    return Array.isArray(body.hits.hits)
    ? body.hits.hits.map(launch => this.launchReducer(launch))
    : [];
  }

  async filterByAuthor(filter, limitRows = { offset: 1, limit: 10}, sortRows = {sort: "title", sortBy: "ASC"}) {
      
      const { body } = await this.client.search({
      index: 'book',
      type: 'books',
      from: limitRows.offset,
      size: limitRows.limit,
      body: {
        "query": {
          "match": {
            "authors": {
              "query": filter.query,
              "operator": "OR"
            }
          }
        }
      }
    })


    return Array.isArray(body.hits.hits)
    ? body.hits.hits.map(launch => this.launchReducer(launch))
    : [];
  }

  async filterByCategory(filter, limitRows = { offset: 1, limit: 10}, sortRows = {sort: "title", sortBy: "ASC"}) {
    const { body } = await this.client.search({
    index: 'book',
    type: 'books',
    from: limitRows.offset,
    size: limitRows.limit,
    body: {
      "query": {
        "match": {
          "categories": {
            "query": filter.query,
            "operator": "OR"
          }
        }
      }
    }
  })


  return Array.isArray(body.hits.hits)
  ? body.hits.hits.map(launch => this.launchReducer(launch))
  : [];
}


  launchReducer(launch) {
    
    launch._source["authors"] = launch._source["authors"].map((item,key)=>{
      return { "name": item };
    });

    launch._source["categories"] = launch._source["categories"].map((item,key)=>{
      return { "name": item };
    });
    
    return launch._source
  }

}

module.exports = BooksSearchApi