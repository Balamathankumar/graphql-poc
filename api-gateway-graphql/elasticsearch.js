'use strict';

const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: process.env.ELASTIC_URL })

const fs = require('fs');

let rawdata = fs.readFileSync('../books.json');
let student = JSON.parse(rawdata);
//console.log(student);


async function insert(student){
    for (let data in student) {
        await client.create({
            index: 'book',
            type: 'books',
            id: data,
            body: student[data]
          });
    }
}

//insert(student);

  async function getData(){

    const { body } = await client.search({
        index: 'book',
        type: 'books',
        // type: '_doc', // uncomment this line if you are using Elasticsearch ≤ 6
        body: {
            "query":{
              "match_all": {
              }
            }
          }
      })
    
      //console.log(body.hits.hits)
      body.hits.hits.forEach(element => {
          console.log(element._source);
      });

  }

  getData();

/*

// callback API
client.search({
    index: 'car',
    body: {  }
  }, (err, result) => {
    if (err) console.log(err)
  })*/
  const { gql } = require('apollo-server-express');
  const GraphQLJSON = require('graphql-type-json');
  gql`
  scalar JSON
  type UserInformation {
      Name: String,
      UserName: String,
      Email: String,
      Language: String,
      Registration: String,
      SkypeName: String,
      Country: String
    }
    type ByProduct
    {        product : String,
        users : Int
    }

    type ByAccount
    {
        account : String,
        users : Int
    }
    
    type UserProfile {
      UserInfo: UserInformation,
      LearnerServed: Int,
      ByProduct: ByProduct,
      ByAccount: ByAccount,
      ByProductAccount: JSON,
      ServiceInfo: ServiceInformation
    }

    type ResponseOutput{
        status: Int,
        message: String
    } 

    input UserUpdateInput {
        learnerId: Int,
        email: String,
        phone1: String,
        phone2: String,
        softPhoneId: String,
        gender : String,
        softPhoneType: String,
        contactPreference: String,
    }
    type Mutation
    {
        LearnerUpdate(params: LearnerUpdateInput) :  ResponseOutput!
    }`;
