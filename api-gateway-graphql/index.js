const express = require('express');
var cors = require('cors')
const { ApolloServer, gql } = require('apollo-server-express');

const BooksSearchApi = require('./src/data_sources/booksearch');
const BooksSearchSchema = require('./src/schema/bookschema');


// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  # This "Book" type defines the queryable fields for every book in our data source.
  ${BooksSearchSchema.BooksSearchTypeDefs}
  
  input Sorting {
    sort: String,
    sortBy: String
  }
  
  input Pagination {
    limit: Int,
    offset: Int,
  }

  input BooksFilterInput {
    query: String
  }

  input BookParamsInput {
    title: String
  }
  

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "books" query returns an array of zero or more Books (defined above).
  type Query {
    books(filter: BooksFilterInput,params: BookParamsInput, limitRows: Pagination, sortRows: Sorting  ): [Book]
    recommendBooksByAuthor(filter: BooksFilterInput, limitRows: Pagination, sortRows: Sorting  ): [RecommendBookByAuthor]
    recommendBooksByCategory(filter: BooksFilterInput, limitRows: Pagination, sortRows: Sorting  ): [RecommendBookByCategory]
  }
  
`;


// Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves books from the "books" array above.
const resolvers = {
    Query: {
        books: async (_, {filter, params, limitRows, sortRows}, { dataSources }) =>{
            var data = await dataSources.booksSearchApi.getBooks(filter, params, limitRows, sortRows);
            return data
        },
        recommendBooksByAuthor: async (_, {filter, limitRows, sortRows}, { dataSources }) =>{
            var data = await dataSources.booksSearchApi.filterByAuthor(filter, limitRows, sortRows);
            return data
        },
        recommendBooksByCategory: async (_, {filter, limitRows, sortRows}, { dataSources }) =>{
            var data = await dataSources.booksSearchApi.filterByCategory(filter, limitRows, sortRows);
            return data
        },
        
    },
  };

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers, playground: true,
    dataSources: () => ({
        booksSearchApi: new BooksSearchApi(),
      }),
});

const app = express();
app.use(
    cors({
      origin: new RegExp(
        process.env.CORS_DOMAIN_NAME || "localhost:3000"
      ),
      methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
      // allowedHeaders: "x-access-token, content-type, Set-Cookie, Set-Cookie2",
      preflightContinue: false
      // credentials: true
    })
  );
server.applyMiddleware({ app });

app.listen({ port: 10025 }, () =>
  console.log(`🚀 Server ready at http://localhost:10025${server.graphqlPath}`)
);